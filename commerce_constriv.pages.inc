<?php

/**
 * @file
 * Manage constriv payment operation.
 */

/**
 * Callback function for payment completed.
 */
function commerce_constriv_complete() {
  $response  = drupal_get_query_parameters(NULL, array('q'), '');

  // Get order id from url. Strip it from test string if necessary.
  $order_id = commerce_constriv_getorderid($response['codTrans']);

  if ($response['esito'] == 'OK') {
    watchdog('commerce_constriv', 'Order %order complete. redirect to complete page', array('%order' => $order_id), WATCHDOG_NOTICE);
    drupal_goto('checkout/' . $order_id . '/complete');
  }

  if ($response['esito'] == 'KO') {
    watchdog('commerce_constriv', 'Order %order KO. redirect to cancelled page', array('%order' => $order_id), WATCHDOG_NOTICE);
    drupal_goto('checkout/constriv/cancelled/' . $order_id);
  }
}

/**
 * Callback function for payment aborted from user.
 */
function commerce_constriv_cancel() {
  $response  = drupal_get_query_parameters(NULL, array('q'), '');

  if (!$response) {
    // We dont have any response from constriv for whatever reason, this could
    // happen when trying to checkout an already precessed order id for now lets
    // just redirect to the front page and log this.
    watchdog('commerce_constriv', 'response from constriv is NULL. User redirected to frontpage', WATCHDOG_ERROR);
    drupal_goto('<front>');
  }
  else {
    // Some parameter are missing from this response.
    $response['mac'] = NULL;
    $response['codAut'] = NULL;

    // Get order id from url. Strip it from test string if necessary.
    $order_id = commerce_constriv_getorderid($response['codTrans']);

    // Load order.
    $order = commerce_order_load($order_id);

    // Redirec to to previous pane.
    commerce_payment_redirect_pane_previous_page($order);

    // Log the transaction attempt as pending.
    commerce_constriv_transaction($order_id, $response, COMMERCE_PAYMENT_STATUS_PENDING);

    watchdog('commerce_constriv', 'response from constriv is %status', array('%status' => $response['esito']), WATCHDOG_NOTICE);

    drupal_goto('checkout/' . $order_id . '/review');

  }
}

/**
 * Callback function for constriv cancelled operation (transaction refused).
 */
function commerce_constriv_cancelled($order) {
  global $user;

  if ($order->uid == $user->uid) {
    $output = array(
      'first' => array(
        '#markup' => t('We are sorry but you order has been cancelled because our payment gateway refused the transaction.<br/> You could try to repeat the checkout process from the beginning. For any question please contact us at %sitemail', array('%sitemail' => variable_get('site_mail', ''))),
      ),
    );

    return $output;
  }
  else {
    drupal_access_denied();
  }
}

/**
 * Callback function for server2server operation from constriv.
 */
function commerce_constriv_server2server() {
  $response  = commerce_constriv_process_response();

  if (!commerce_constrive_server2server_validation($response)) {
    watchdog('commerce_constriv', 'Error mac validation server2server. !response !session', array(
        '!response' => print_r($response, TRUE),
        '!session'  => print_r($_SESSION, TRUE),
      ), WATCHDOG_ERROR);
    drupal_access_denied();
    return;
  }


  watchdog('commerce_constriv', 'response from constriv is %status', array('%status' => $response['esito']), WATCHDOG_NOTICE);

  // Get order id from url. Strip it from test string if necessary.
  $order_id = commerce_constriv_getorderid($response['codTrans']);

  // Load order.
  $order = commerce_order_load($order_id);

  if ($response['esito'] == 'OK') {
    commerce_payment_redirect_pane_next_page($order);
    $status = COMMERCE_PAYMENT_STATUS_SUCCESS;
  }

  if ($response['esito'] == 'KO') {
    // No moving to previus pane here cause the PG would not accept an other
    // attempt with the same order id.
    commerce_order_status_update($order, 'canceled', FALSE, TRUE, 'Transaction refused by constriv Payment Gateway');
    $status = COMMERCE_PAYMENT_STATUS_FAILURE;
  }

  // Log the transaction attempt.
  commerce_constriv_transaction($order_id, $response, $status);

  return '';
}

